#include <stdio.h>
#include <math.h>
#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm>    

using namespace std;

#define R 1
#define W 1
#define E 1
#define V 1
#define BYTE 8

size_t start_address;
int page_size, vir, phy, offset, ignored_r, frame_number, ignored_l, entry_size;
std::vector<int> pointer, address, pointer_bin, indizes;

void printData();
void getEntrySize();
void getFrameNumber();
void getIgnoredLeft();
void getIgnoredRight();
void print(std::vector<int> vec);
void calcEntries();
vector<int> hex_to_bin(string ptr);
void convertPointer();
void cutPointer();
void getIndex();
void getStartAdress();
int getPPN(vector<int> entry);
void cutEntry(vector<int> &entry);
void getEntries();
void printRWEV(size_t address, vector<int> data);
string getRAMEntry(size_t address);

int main(int argc, char** argv)
{
    if(argc != 4)
    {
        cout << "./a.out virtual_bits physical_bits page_size" << endl;
        return 0;
    }

    vir = atoi(argv[1]);
    phy = atoi(argv[2]);
    page_size = atoi(argv[3]);
    offset = log2(page_size);

    getEntrySize();
    getFrameNumber();
    getIgnoredRight();
    getIgnoredLeft();
    printData();
    calcEntries();
    convertPointer();
    cutPointer();
    getIndex();
    getStartAdress();
    getEntries();

    return 0;
}

void print(std::vector<int> vec)
{
    for(auto e: vec)
    {
        cout << e << " ";
    }
    cout << endl;
}

void getIgnoredRight()
{
    ignored_r = offset - R - W - E - V;
}

void getIgnoredLeft()
{
    ignored_l = entry_size * BYTE - phy;
}

void getFrameNumber()
{
    frame_number = phy - offset;
}

void getEntrySize()
{
    if(phy <= 64)
    {
        entry_size = 8;
    }
    if(phy <= 32)
    {
        entry_size = 4;
    }
    if(phy <= 16)
    {
        entry_size = 2;
    }
}

void printData()
{
    cout << "Ignored | Frame number | Ignored | R | W | E | V " << endl;
    cout << ignored_l << "         " << frame_number;
    cout << "             " << ignored_r << "         ";
    cout << "1   1   1   1" << endl;
    cout << "Entry size: " << entry_size << " bytes" << endl;
}

void calcEntries()
{
    int max_entries = page_size / entry_size;
    int max_entries_bits = log2(max_entries);    
    int bit_left_for_index = vir - offset;

    int full_entries = bit_left_for_index / max_entries_bits;
    pointer.push_back(bit_left_for_index - full_entries * max_entries_bits);
    int top_lvl = pow(2,bit_left_for_index - full_entries * max_entries_bits);
    int lvl = 0;
    for(; lvl < full_entries; lvl++)
    {
        cout << "pml" << lvl+1 << ": " << max_entries << "| ";
        pointer.push_back(max_entries_bits);
    }
    cout << "pml" << lvl+1 << ": " << top_lvl << endl;
    cout << "Levels: " << pointer.size() << endl;
    pointer.push_back(offset);
}

vector<int> hex_to_bin(string ptr)
{
  vector<int> tmp;
  for(auto c: ptr)
  {
    switch(toupper(c))
    {
        case '0': 
        {
            tmp.push_back(0);
            tmp.push_back(0);
            tmp.push_back(0);
            tmp.push_back(0);
        } 
        break;
        case '1':
        {
            tmp.push_back(0);
            tmp.push_back(0);
            tmp.push_back(0);
            tmp.push_back(1);
        } 
        break;
        case '2':
        {
            tmp.push_back(0);
            tmp.push_back(0);
            tmp.push_back(1);
            tmp.push_back(0);
        } 
        break;
        case '3':
        {
            tmp.push_back(0);
            tmp.push_back(0);
            tmp.push_back(1);
            tmp.push_back(1);
        } 
        break;
        case '4':
        {
            tmp.push_back(0);
            tmp.push_back(1);
            tmp.push_back(0);
            tmp.push_back(0);
        }
        break;
        case '5':
        {
            tmp.push_back(0);
            tmp.push_back(1);
            tmp.push_back(0);
            tmp.push_back(1);
        }
        break;
        case '6':
        {
            tmp.push_back(0);
            tmp.push_back(1);
            tmp.push_back(1);
            tmp.push_back(0);
        }
        break;
        case '7':
        {
            tmp.push_back(0);
            tmp.push_back(1);
            tmp.push_back(1);
            tmp.push_back(1);
        }
        break;
        case '8':
        {
            tmp.push_back(1);
            tmp.push_back(0);
            tmp.push_back(0);
            tmp.push_back(0);
        }
        break;
        case '9':
        {
            tmp.push_back(1);
            tmp.push_back(0);
            tmp.push_back(0);
            tmp.push_back(1);
        }
        break;
        case 'A':
        {
            tmp.push_back(1);
            tmp.push_back(0);
            tmp.push_back(1);
            tmp.push_back(0);
        }
        break;
        case 'B':
        {
            tmp.push_back(1);
            tmp.push_back(0);
            tmp.push_back(1);
            tmp.push_back(1);
        }
        break;
        case 'C':
        {
            tmp.push_back(1);
            tmp.push_back(1);
            tmp.push_back(0);
            tmp.push_back(0);
        }
        break;
        case 'D':
        {
            tmp.push_back(1);
            tmp.push_back(1);
            tmp.push_back(0);
            tmp.push_back(1);
        }
        break;
        case 'E':
        {
            tmp.push_back(1);
            tmp.push_back(1);
            tmp.push_back(1);
            tmp.push_back(0);
        }
        break;
        case 'F':
        {
            tmp.push_back(1);
            tmp.push_back(1);
            tmp.push_back(1);
            tmp.push_back(1);
        }
        break;
    }
  }
  return tmp;
}

int bin_to_dec(string bin)
{
    int dec = 0;
    int power = 0;
    for(int i = 0; i < bin.length(); i++)
    {
        dec = dec + (bin.at(i)-48) * pow(2, power);
        power++;
    }
    return dec;
}

void convertPointer()
{
    string ptr;
    cout << "Enter pointer: 0x";
    cin >> ptr;
    pointer_bin = hex_to_bin(ptr);
}

void cutPointer()
{
    int diff = pointer_bin.size() - vir;
    if(diff < 0)
    {
        int add = diff * -1;
        for(int i = 0; i < add; i++)
        {
            pointer_bin.insert(pointer_bin.begin(), 1, 0);
        }
        return;
    }

    for(int i = 0; i < diff; i++)
    {
        pointer_bin.erase(pointer_bin.begin());
    }
}

void getIndex()
{
    stringstream ss;
    int off = 0;

    for(auto idx: pointer)
    {
        for(int i = 0; i < idx; i++)
        {
            ss << pointer_bin.at(i + off);
        }
        string tmp = ss.str();
        reverse(tmp.begin(), tmp.end());
        int index = bin_to_dec(tmp);
        ss.str("");
        indizes.push_back(index);
        off += idx;
    }
    cout << "Indizes: ";
    for(auto index: indizes)
    {
        printf("0x%x | ", index);
    }
    cout << endl;
}

void getStartAdress()
{
    string start;
    cout << "Enter startaddress: 0x";
    cin >> start;
    vector<int> add = hex_to_bin(start);
    stringstream ss;
    for(auto s: add)
    {
        ss << s;
    }
    string tmp = ss.str();
    reverse(tmp.begin(), tmp.end());
    start_address = bin_to_dec(tmp);
}

int getPPN(vector<int> entry)
{
    for(int i = 0; i < offset; i++)
    {
        entry.pop_back();
    }

    for(int i = 0; i < ignored_l; i++)
    {
        entry.erase(entry.begin());
    }
    stringstream ss;
    for(auto c: entry)
    {
        ss << c;
    }
    string tmp = ss.str();
    reverse(tmp.begin(), tmp.end());
    size_t ppn = bin_to_dec(tmp);
    printf("PPN: 0x%lx \n", ppn);
    return ppn;
} 

void cutEntry(vector<int> &entry)
{
    int diff = entry.size() - phy;
    for(int i = 0; i < diff; i++)
    {
        entry.at(i) = 0;
    }
}

void getEntries()
{
    size_t address = start_address;
    int shift = log2(entry_size);
    vector<int> entry_to_bin;
    for(int i = 0; i < indizes.size(); i++)
    {
        if(i == indizes.size() - 1)
        {
            address += indizes.at(i);
            printRWEV(address, entry_to_bin);
            break;
        }
        address += (indizes.at(i) << shift);
        string entry = getRAMEntry(address);
        entry_to_bin = hex_to_bin(entry);
        if(entry.size()*4 > phy)
            cutEntry(entry_to_bin);
        int ppn = getPPN(entry_to_bin);
        address = ppn << offset;
    }
}

void printRWEV(size_t address, vector<int> data)
{
    printf("offset: 0x%lx \n", (size_t)indizes.at(indizes.size()-1));
    cout << "read: " <<  data.at(data.size() - 4) << " ";
    cout << "write: " <<  data.at(data.size() - 3) << " ";
    cout << "exec: " <<  data.at(data.size() - 2) << " ";
    cout << "valid: " <<  data.at(data.size() - 1) << endl;
    printf("physical address: 0x%lx \n", address);
}

string getRAMEntry(size_t address)
{
    string entry;
    printf("Enter entry of address 0x%lx: ", address);
    while(entry.size() != entry_size * 2)
    {
        cin >> entry;
        if(entry.size() != entry_size * 2)
        {
        printf("Enter entry of address 0x%lx: ", address);
        }
    }
    return entry;
}



